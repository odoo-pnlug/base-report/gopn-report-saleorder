# -*- coding: utf-8 -*-
{
    'name': 'GOPN-SALE-ORDER-REPORT',
    'summary': 'Report template for Customer',
    'description': "This report has customized for Customer",
    'author': "GOPN Gruppo Odoo PnLug",
    'website': "https://odoo.pnlug.it",
    'category': 'Uncategorized',
    'version': '10.0.0.1',
    'license': 'AGPL-3',
    'depends': [
        'base',
        'sale',
        'report',
        'sale_order_dates',
    ],
    'data': [
        "data/paper_format.xml",
        "views/sale_order_report.xml",
    ],
    'installable': True,
    'auto_install': False,
}
